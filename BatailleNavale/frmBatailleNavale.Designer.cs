﻿namespace BatailleNavale
{
    partial class frmBatailleNavale
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBatailleNavale));
			this.grpOrdinateur = new System.Windows.Forms.GroupBox();
			this.lblOrdiTorpilleur = new System.Windows.Forms.Label();
			this.lblOrdiSousMarin = new System.Windows.Forms.Label();
			this.lblOrdiCroiseur = new System.Windows.Forms.Label();
			this.lblOrdiCuirassé = new System.Windows.Forms.Label();
			this.lblOrdiPorteAvions = new System.Windows.Forms.Label();
			this.optOrdiIntelligent = new System.Windows.Forms.RadioButton();
			this.optOrdiHasard = new System.Windows.Forms.RadioButton();
			this.lblOrdiMode = new System.Windows.Forms.Label();
			this.lblOrdiAttaqueCoordonnée = new System.Windows.Forms.Label();
			this.lblOrdiAttaqueStatut = new System.Windows.Forms.Label();
			this.lblOrdiStatut = new System.Windows.Forms.Label();
			this.lblOrdiAttaqueCoordonnéeStatique = new System.Windows.Forms.Label();
			this.vaiOrdinateur = new VisualArrays.VisualIntArray();
			this.imgCase = new System.Windows.Forms.ImageList(this.components);
			this.grpJoueur = new System.Windows.Forms.GroupBox();
			this.grpJoueurAttaques = new System.Windows.Forms.GroupBox();
			this.lblJoueurAttaqueStatut = new System.Windows.Forms.Label();
			this.lblJoueurAttaqueStatutStatique = new System.Windows.Forms.Label();
			this.txtJoueurAttaquesCoord = new System.Windows.Forms.TextBox();
			this.lblJoueurAttCoordonnée = new System.Windows.Forms.Label();
			this.chkJoueurTorpilleur = new System.Windows.Forms.CheckBox();
			this.chkJoueurSousMarin = new System.Windows.Forms.CheckBox();
			this.chkJoueurCroiseur = new System.Windows.Forms.CheckBox();
			this.chkJoueurCuirassé = new System.Windows.Forms.CheckBox();
			this.lblJoueurVertical = new System.Windows.Forms.Label();
			this.chkJoueurPorteAvions = new System.Windows.Forms.CheckBox();
			this.lblCoord = new System.Windows.Forms.Label();
			this.txtJoueurTorpilleur = new System.Windows.Forms.TextBox();
			this.txtJoueurSousMarin = new System.Windows.Forms.TextBox();
			this.txtJoueurCroiseur = new System.Windows.Forms.TextBox();
			this.txtJoueurCuirassé = new System.Windows.Forms.TextBox();
			this.txtJoueurPorteAvions = new System.Windows.Forms.TextBox();
			this.lblJoueurTorpilleur = new System.Windows.Forms.Label();
			this.lblJoueurSousMarin = new System.Windows.Forms.Label();
			this.lblJoueurCroiseur = new System.Windows.Forms.Label();
			this.lblJoueurCuirassé = new System.Windows.Forms.Label();
			this.lblJoueurPorteAvions = new System.Windows.Forms.Label();
			this.vaiJoueur = new VisualArrays.VisualIntArray();
			this.btnDémarrer = new System.Windows.Forms.Button();
			this.btnRecommencer = new System.Windows.Forms.Button();
			this.grpOrdinateur.SuspendLayout();
			this.grpJoueur.SuspendLayout();
			this.grpJoueurAttaques.SuspendLayout();
			this.SuspendLayout();
			// 
			// grpOrdinateur
			// 
			this.grpOrdinateur.BackColor = System.Drawing.Color.Transparent;
			this.grpOrdinateur.Controls.Add(this.lblOrdiTorpilleur);
			this.grpOrdinateur.Controls.Add(this.lblOrdiSousMarin);
			this.grpOrdinateur.Controls.Add(this.lblOrdiCroiseur);
			this.grpOrdinateur.Controls.Add(this.lblOrdiCuirassé);
			this.grpOrdinateur.Controls.Add(this.lblOrdiPorteAvions);
			this.grpOrdinateur.Controls.Add(this.optOrdiIntelligent);
			this.grpOrdinateur.Controls.Add(this.optOrdiHasard);
			this.grpOrdinateur.Controls.Add(this.lblOrdiMode);
			this.grpOrdinateur.Controls.Add(this.lblOrdiAttaqueCoordonnée);
			this.grpOrdinateur.Controls.Add(this.lblOrdiAttaqueStatut);
			this.grpOrdinateur.Controls.Add(this.lblOrdiStatut);
			this.grpOrdinateur.Controls.Add(this.lblOrdiAttaqueCoordonnéeStatique);
			this.grpOrdinateur.Controls.Add(this.vaiOrdinateur);
			this.grpOrdinateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.grpOrdinateur.ForeColor = System.Drawing.Color.White;
			this.grpOrdinateur.Location = new System.Drawing.Point(506, 2);
			this.grpOrdinateur.Margin = new System.Windows.Forms.Padding(5);
			this.grpOrdinateur.Name = "grpOrdinateur";
			this.grpOrdinateur.Size = new System.Drawing.Size(497, 620);
			this.grpOrdinateur.TabIndex = 1;
			this.grpOrdinateur.TabStop = false;
			this.grpOrdinateur.Text = "Ordinateur:";
			// 
			// lblOrdiTorpilleur
			// 
			this.lblOrdiTorpilleur.AutoSize = true;
			this.lblOrdiTorpilleur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiTorpilleur.Location = new System.Drawing.Point(353, 594);
			this.lblOrdiTorpilleur.Name = "lblOrdiTorpilleur";
			this.lblOrdiTorpilleur.Size = new System.Drawing.Size(90, 20);
			this.lblOrdiTorpilleur.TabIndex = 6;
			this.lblOrdiTorpilleur.Text = "5. Torpilleur";
			// 
			// lblOrdiSousMarin
			// 
			this.lblOrdiSousMarin.AutoSize = true;
			this.lblOrdiSousMarin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiSousMarin.Location = new System.Drawing.Point(353, 574);
			this.lblOrdiSousMarin.Name = "lblOrdiSousMarin";
			this.lblOrdiSousMarin.Size = new System.Drawing.Size(107, 20);
			this.lblOrdiSousMarin.TabIndex = 13;
			this.lblOrdiSousMarin.Text = "4. Sous-marin";
			// 
			// lblOrdiCroiseur
			// 
			this.lblOrdiCroiseur.AutoSize = true;
			this.lblOrdiCroiseur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiCroiseur.Location = new System.Drawing.Point(353, 554);
			this.lblOrdiCroiseur.Name = "lblOrdiCroiseur";
			this.lblOrdiCroiseur.Size = new System.Drawing.Size(85, 20);
			this.lblOrdiCroiseur.TabIndex = 12;
			this.lblOrdiCroiseur.Text = "3. Croiseur";
			// 
			// lblOrdiCuirassé
			// 
			this.lblOrdiCuirassé.AutoSize = true;
			this.lblOrdiCuirassé.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiCuirassé.Location = new System.Drawing.Point(353, 534);
			this.lblOrdiCuirassé.Name = "lblOrdiCuirassé";
			this.lblOrdiCuirassé.Size = new System.Drawing.Size(88, 20);
			this.lblOrdiCuirassé.TabIndex = 11;
			this.lblOrdiCuirassé.Text = "2. Cuirassé";
			// 
			// lblOrdiPorteAvions
			// 
			this.lblOrdiPorteAvions.AutoSize = true;
			this.lblOrdiPorteAvions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiPorteAvions.Location = new System.Drawing.Point(353, 514);
			this.lblOrdiPorteAvions.Name = "lblOrdiPorteAvions";
			this.lblOrdiPorteAvions.Size = new System.Drawing.Size(114, 20);
			this.lblOrdiPorteAvions.TabIndex = 10;
			this.lblOrdiPorteAvions.Text = "1. Porte-avions";
			// 
			// optOrdiIntelligent
			// 
			this.optOrdiIntelligent.AutoSize = true;
			this.optOrdiIntelligent.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.optOrdiIntelligent.Location = new System.Drawing.Point(249, 570);
			this.optOrdiIntelligent.Name = "optOrdiIntelligent";
			this.optOrdiIntelligent.Size = new System.Drawing.Size(96, 24);
			this.optOrdiIntelligent.TabIndex = 9;
			this.optOrdiIntelligent.Text = "Intelligent";
			this.optOrdiIntelligent.UseVisualStyleBackColor = true;
			// 
			// optOrdiHasard
			// 
			this.optOrdiHasard.AutoSize = true;
			this.optOrdiHasard.Checked = true;
			this.optOrdiHasard.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.optOrdiHasard.Location = new System.Drawing.Point(249, 550);
			this.optOrdiHasard.Name = "optOrdiHasard";
			this.optOrdiHasard.Size = new System.Drawing.Size(79, 24);
			this.optOrdiHasard.TabIndex = 8;
			this.optOrdiHasard.TabStop = true;
			this.optOrdiHasard.Text = "Hasard";
			this.optOrdiHasard.UseVisualStyleBackColor = true;
			// 
			// lblOrdiMode
			// 
			this.lblOrdiMode.AutoSize = true;
			this.lblOrdiMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiMode.Location = new System.Drawing.Point(245, 531);
			this.lblOrdiMode.Name = "lblOrdiMode";
			this.lblOrdiMode.Size = new System.Drawing.Size(53, 20);
			this.lblOrdiMode.TabIndex = 7;
			this.lblOrdiMode.Text = "Mode:";
			// 
			// lblOrdiAttaqueCoordonnée
			// 
			this.lblOrdiAttaqueCoordonnée.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblOrdiAttaqueCoordonnée.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiAttaqueCoordonnée.Location = new System.Drawing.Point(20, 555);
			this.lblOrdiAttaqueCoordonnée.Name = "lblOrdiAttaqueCoordonnée";
			this.lblOrdiAttaqueCoordonnée.Size = new System.Drawing.Size(82, 34);
			this.lblOrdiAttaqueCoordonnée.TabIndex = 6;
			this.lblOrdiAttaqueCoordonnée.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblOrdiAttaqueStatut
			// 
			this.lblOrdiAttaqueStatut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblOrdiAttaqueStatut.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiAttaqueStatut.Location = new System.Drawing.Point(124, 555);
			this.lblOrdiAttaqueStatut.Name = "lblOrdiAttaqueStatut";
			this.lblOrdiAttaqueStatut.Size = new System.Drawing.Size(112, 34);
			this.lblOrdiAttaqueStatut.TabIndex = 5;
			this.lblOrdiAttaqueStatut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblOrdiStatut
			// 
			this.lblOrdiStatut.AutoSize = true;
			this.lblOrdiStatut.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiStatut.Location = new System.Drawing.Point(119, 531);
			this.lblOrdiStatut.Name = "lblOrdiStatut";
			this.lblOrdiStatut.Size = new System.Drawing.Size(57, 20);
			this.lblOrdiStatut.TabIndex = 4;
			this.lblOrdiStatut.Text = "Statut:";
			// 
			// lblOrdiAttaqueCoordonnéeStatique
			// 
			this.lblOrdiAttaqueCoordonnéeStatique.AutoSize = true;
			this.lblOrdiAttaqueCoordonnéeStatique.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblOrdiAttaqueCoordonnéeStatique.Location = new System.Drawing.Point(16, 531);
			this.lblOrdiAttaqueCoordonnéeStatique.Name = "lblOrdiAttaqueCoordonnéeStatique";
			this.lblOrdiAttaqueCoordonnéeStatique.Size = new System.Drawing.Size(101, 20);
			this.lblOrdiAttaqueCoordonnéeStatique.TabIndex = 2;
			this.lblOrdiAttaqueCoordonnéeStatique.Text = "Coordonnée:";
			// 
			// vaiOrdinateur
			// 
			this.vaiOrdinateur.BackColor = System.Drawing.Color.Transparent;
			this.vaiOrdinateur.BackgroundImage = global::BatailleNavale.Properties.Resources.mer3_475X475;
			this.vaiOrdinateur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.vaiOrdinateur.CellAppearance.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiOrdinateur.CellAppearance.ImageList = this.imgCase;
			this.vaiOrdinateur.CellAppearance.TextColor = System.Drawing.Color.Black;
			this.vaiOrdinateur.CellSize = new System.Drawing.Size(40, 40);
			this.vaiOrdinateur.ColumnCount = 10;
			this.vaiOrdinateur.ColumnHeader.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiOrdinateur.ColumnHeader.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.vaiOrdinateur.ColumnHeader.ForeColor = System.Drawing.Color.Black;
			this.vaiOrdinateur.ColumnHeader.Style = VisualArrays.enuHeaderBkgStyle.None;
			this.vaiOrdinateur.ColumnHeader.ValueStyle = VisualArrays.enuDataStyle.IndexBase1;
			this.vaiOrdinateur.ColumnHeader.Visible = true;
			this.vaiOrdinateur.Enabled = false;
			this.vaiOrdinateur.GridAppearance.Color = System.Drawing.Color.Black;
			this.vaiOrdinateur.GridAppearance.Margin = 3;
			this.vaiOrdinateur.Location = new System.Drawing.Point(7, 31);
			this.vaiOrdinateur.Name = "vaiOrdinateur";
			this.vaiOrdinateur.RowCount = 10;
			this.vaiOrdinateur.RowHeader.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiOrdinateur.RowHeader.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.vaiOrdinateur.RowHeader.ForeColor = System.Drawing.Color.Black;
			this.vaiOrdinateur.RowHeader.Style = VisualArrays.enuHeaderBkgStyle.None;
			this.vaiOrdinateur.RowHeader.ValueStyle = VisualArrays.enuDataStyle.User;
			this.vaiOrdinateur.RowHeader.Visible = true;
			this.vaiOrdinateur.Size = new System.Drawing.Size(477, 477);
			this.vaiOrdinateur.TabIndex = 1;
			this.vaiOrdinateur.View = VisualArrays.enuIntView.ImageList;
			this.vaiOrdinateur.CellMouseClick += new System.EventHandler<VisualArrays.CellMouseEventArgs>(this.vaiOrdinateur_CellMouseClick);
			// 
			// imgCase
			// 
			this.imgCase.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgCase.ImageStream")));
			this.imgCase.TransparentColor = System.Drawing.Color.Transparent;
			this.imgCase.Images.SetKeyName(0, "Vide.png");
			this.imgCase.Images.SetKeyName(1, "À l\'eau.png");
			this.imgCase.Images.SetKeyName(2, "Touché.png");
			this.imgCase.Images.SetKeyName(3, "TouchéSansBateau.png");
			this.imgCase.Images.SetKeyName(4, "Bateau.png");
			// 
			// grpJoueur
			// 
			this.grpJoueur.BackColor = System.Drawing.Color.Transparent;
			this.grpJoueur.Controls.Add(this.grpJoueurAttaques);
			this.grpJoueur.Controls.Add(this.chkJoueurTorpilleur);
			this.grpJoueur.Controls.Add(this.chkJoueurSousMarin);
			this.grpJoueur.Controls.Add(this.chkJoueurCroiseur);
			this.grpJoueur.Controls.Add(this.chkJoueurCuirassé);
			this.grpJoueur.Controls.Add(this.lblJoueurVertical);
			this.grpJoueur.Controls.Add(this.chkJoueurPorteAvions);
			this.grpJoueur.Controls.Add(this.lblCoord);
			this.grpJoueur.Controls.Add(this.txtJoueurTorpilleur);
			this.grpJoueur.Controls.Add(this.txtJoueurSousMarin);
			this.grpJoueur.Controls.Add(this.txtJoueurCroiseur);
			this.grpJoueur.Controls.Add(this.txtJoueurCuirassé);
			this.grpJoueur.Controls.Add(this.txtJoueurPorteAvions);
			this.grpJoueur.Controls.Add(this.lblJoueurTorpilleur);
			this.grpJoueur.Controls.Add(this.lblJoueurSousMarin);
			this.grpJoueur.Controls.Add(this.lblJoueurCroiseur);
			this.grpJoueur.Controls.Add(this.lblJoueurCuirassé);
			this.grpJoueur.Controls.Add(this.lblJoueurPorteAvions);
			this.grpJoueur.Controls.Add(this.vaiJoueur);
			this.grpJoueur.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.grpJoueur.ForeColor = System.Drawing.Color.White;
			this.grpJoueur.Location = new System.Drawing.Point(6, 2);
			this.grpJoueur.Name = "grpJoueur";
			this.grpJoueur.Size = new System.Drawing.Size(497, 674);
			this.grpJoueur.TabIndex = 0;
			this.grpJoueur.TabStop = false;
			this.grpJoueur.Text = "Joueur:";
			// 
			// grpJoueurAttaques
			// 
			this.grpJoueurAttaques.Controls.Add(this.lblJoueurAttaqueStatut);
			this.grpJoueurAttaques.Controls.Add(this.lblJoueurAttaqueStatutStatique);
			this.grpJoueurAttaques.Controls.Add(this.txtJoueurAttaquesCoord);
			this.grpJoueurAttaques.Controls.Add(this.lblJoueurAttCoordonnée);
			this.grpJoueurAttaques.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.grpJoueurAttaques.ForeColor = System.Drawing.Color.White;
			this.grpJoueurAttaques.Location = new System.Drawing.Point(304, 514);
			this.grpJoueurAttaques.Name = "grpJoueurAttaques";
			this.grpJoueurAttaques.Size = new System.Drawing.Size(179, 150);
			this.grpJoueurAttaques.TabIndex = 18;
			this.grpJoueurAttaques.TabStop = false;
			this.grpJoueurAttaques.Text = "Attaques:";
			// 
			// lblJoueurAttaqueStatut
			// 
			this.lblJoueurAttaqueStatut.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.lblJoueurAttaqueStatut.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurAttaqueStatut.Location = new System.Drawing.Point(11, 110);
			this.lblJoueurAttaqueStatut.Name = "lblJoueurAttaqueStatut";
			this.lblJoueurAttaqueStatut.Size = new System.Drawing.Size(159, 34);
			this.lblJoueurAttaqueStatut.TabIndex = 3;
			this.lblJoueurAttaqueStatut.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblJoueurAttaqueStatutStatique
			// 
			this.lblJoueurAttaqueStatutStatique.AutoSize = true;
			this.lblJoueurAttaqueStatutStatique.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurAttaqueStatutStatique.Location = new System.Drawing.Point(6, 86);
			this.lblJoueurAttaqueStatutStatique.Name = "lblJoueurAttaqueStatutStatique";
			this.lblJoueurAttaqueStatutStatique.Size = new System.Drawing.Size(57, 20);
			this.lblJoueurAttaqueStatutStatique.TabIndex = 2;
			this.lblJoueurAttaqueStatutStatique.Text = "Statut:";
			// 
			// txtJoueurAttaquesCoord
			// 
			this.txtJoueurAttaquesCoord.AcceptsReturn = true;
			this.txtJoueurAttaquesCoord.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurAttaquesCoord.Enabled = false;
			this.txtJoueurAttaquesCoord.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurAttaquesCoord.Location = new System.Drawing.Point(10, 52);
			this.txtJoueurAttaquesCoord.MaxLength = 3;
			this.txtJoueurAttaquesCoord.Name = "txtJoueurAttaquesCoord";
			this.txtJoueurAttaquesCoord.Size = new System.Drawing.Size(160, 26);
			this.txtJoueurAttaquesCoord.TabIndex = 1;
			this.txtJoueurAttaquesCoord.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// lblJoueurAttCoordonnée
			// 
			this.lblJoueurAttCoordonnée.AutoSize = true;
			this.lblJoueurAttCoordonnée.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurAttCoordonnée.Location = new System.Drawing.Point(6, 28);
			this.lblJoueurAttCoordonnée.Name = "lblJoueurAttCoordonnée";
			this.lblJoueurAttCoordonnée.Size = new System.Drawing.Size(164, 20);
			this.lblJoueurAttCoordonnée.TabIndex = 0;
			this.lblJoueurAttCoordonnée.Text = "Coordonnée (ex. :D8):";
			// 
			// chkJoueurTorpilleur
			// 
			this.chkJoueurTorpilleur.AutoSize = true;
			this.chkJoueurTorpilleur.Location = new System.Drawing.Point(250, 645);
			this.chkJoueurTorpilleur.Name = "chkJoueurTorpilleur";
			this.chkJoueurTorpilleur.Size = new System.Drawing.Size(15, 14);
			this.chkJoueurTorpilleur.TabIndex = 17;
			this.chkJoueurTorpilleur.UseVisualStyleBackColor = true;
			this.chkJoueurTorpilleur.CheckedChanged += new System.EventHandler(this.chkTorpilleur_CheckedChanged);
			// 
			// chkJoueurSousMarin
			// 
			this.chkJoueurSousMarin.AutoSize = true;
			this.chkJoueurSousMarin.Location = new System.Drawing.Point(250, 618);
			this.chkJoueurSousMarin.Name = "chkJoueurSousMarin";
			this.chkJoueurSousMarin.Size = new System.Drawing.Size(15, 14);
			this.chkJoueurSousMarin.TabIndex = 16;
			this.chkJoueurSousMarin.UseVisualStyleBackColor = true;
			this.chkJoueurSousMarin.CheckedChanged += new System.EventHandler(this.chkSousMarin_CheckedChanged);
			// 
			// chkJoueurCroiseur
			// 
			this.chkJoueurCroiseur.AutoSize = true;
			this.chkJoueurCroiseur.Location = new System.Drawing.Point(250, 592);
			this.chkJoueurCroiseur.Name = "chkJoueurCroiseur";
			this.chkJoueurCroiseur.Size = new System.Drawing.Size(15, 14);
			this.chkJoueurCroiseur.TabIndex = 15;
			this.chkJoueurCroiseur.UseVisualStyleBackColor = true;
			this.chkJoueurCroiseur.CheckedChanged += new System.EventHandler(this.chkCroiseur_CheckedChanged);
			// 
			// chkJoueurCuirassé
			// 
			this.chkJoueurCuirassé.AutoSize = true;
			this.chkJoueurCuirassé.Location = new System.Drawing.Point(250, 567);
			this.chkJoueurCuirassé.Name = "chkJoueurCuirassé";
			this.chkJoueurCuirassé.Size = new System.Drawing.Size(15, 14);
			this.chkJoueurCuirassé.TabIndex = 14;
			this.chkJoueurCuirassé.UseVisualStyleBackColor = true;
			this.chkJoueurCuirassé.CheckedChanged += new System.EventHandler(this.chkCuirassé_CheckedChanged);
			// 
			// lblJoueurVertical
			// 
			this.lblJoueurVertical.AutoSize = true;
			this.lblJoueurVertical.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurVertical.Location = new System.Drawing.Point(248, 514);
			this.lblJoueurVertical.Name = "lblJoueurVertical";
			this.lblJoueurVertical.Size = new System.Drawing.Size(24, 16);
			this.lblJoueurVertical.TabIndex = 13;
			this.lblJoueurVertical.Text = "V?";
			// 
			// chkJoueurPorteAvions
			// 
			this.chkJoueurPorteAvions.AutoSize = true;
			this.chkJoueurPorteAvions.Location = new System.Drawing.Point(250, 542);
			this.chkJoueurPorteAvions.Name = "chkJoueurPorteAvions";
			this.chkJoueurPorteAvions.Size = new System.Drawing.Size(15, 14);
			this.chkJoueurPorteAvions.TabIndex = 12;
			this.chkJoueurPorteAvions.UseVisualStyleBackColor = true;
			this.chkJoueurPorteAvions.CheckedChanged += new System.EventHandler(this.chkPorteAvions_CheckedChanged);
			// 
			// lblCoord
			// 
			this.lblCoord.AutoSize = true;
			this.lblCoord.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblCoord.Location = new System.Drawing.Point(191, 514);
			this.lblCoord.Name = "lblCoord";
			this.lblCoord.Size = new System.Drawing.Size(51, 16);
			this.lblCoord.TabIndex = 11;
			this.lblCoord.Text = "Coord.:";
			// 
			// txtJoueurTorpilleur
			// 
			this.txtJoueurTorpilleur.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurTorpilleur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurTorpilleur.Location = new System.Drawing.Point(199, 640);
			this.txtJoueurTorpilleur.MaxLength = 3;
			this.txtJoueurTorpilleur.Name = "txtJoueurTorpilleur";
			this.txtJoueurTorpilleur.Size = new System.Drawing.Size(35, 26);
			this.txtJoueurTorpilleur.TabIndex = 10;
			this.txtJoueurTorpilleur.TextChanged += new System.EventHandler(this.txtTorpilleur_TextChanged);
			// 
			// txtJoueurSousMarin
			// 
			this.txtJoueurSousMarin.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurSousMarin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurSousMarin.Location = new System.Drawing.Point(199, 614);
			this.txtJoueurSousMarin.MaxLength = 3;
			this.txtJoueurSousMarin.Name = "txtJoueurSousMarin";
			this.txtJoueurSousMarin.Size = new System.Drawing.Size(35, 26);
			this.txtJoueurSousMarin.TabIndex = 9;
			this.txtJoueurSousMarin.TextChanged += new System.EventHandler(this.txtSousMarin_TextChanged);
			// 
			// txtJoueurCroiseur
			// 
			this.txtJoueurCroiseur.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurCroiseur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurCroiseur.Location = new System.Drawing.Point(199, 588);
			this.txtJoueurCroiseur.MaxLength = 3;
			this.txtJoueurCroiseur.Name = "txtJoueurCroiseur";
			this.txtJoueurCroiseur.Size = new System.Drawing.Size(35, 26);
			this.txtJoueurCroiseur.TabIndex = 8;
			this.txtJoueurCroiseur.TextChanged += new System.EventHandler(this.txtCroiseur_TextChanged);
			// 
			// txtJoueurCuirassé
			// 
			this.txtJoueurCuirassé.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurCuirassé.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurCuirassé.Location = new System.Drawing.Point(199, 562);
			this.txtJoueurCuirassé.MaxLength = 3;
			this.txtJoueurCuirassé.Name = "txtJoueurCuirassé";
			this.txtJoueurCuirassé.Size = new System.Drawing.Size(35, 26);
			this.txtJoueurCuirassé.TabIndex = 7;
			this.txtJoueurCuirassé.TextChanged += new System.EventHandler(this.txtCuirassé_TextChanged);
			// 
			// txtJoueurPorteAvions
			// 
			this.txtJoueurPorteAvions.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
			this.txtJoueurPorteAvions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txtJoueurPorteAvions.Location = new System.Drawing.Point(199, 537);
			this.txtJoueurPorteAvions.MaxLength = 3;
			this.txtJoueurPorteAvions.Name = "txtJoueurPorteAvions";
			this.txtJoueurPorteAvions.Size = new System.Drawing.Size(35, 26);
			this.txtJoueurPorteAvions.TabIndex = 6;
			this.txtJoueurPorteAvions.TextChanged += new System.EventHandler(this.txtPorteAvions_TextChanged);
			// 
			// lblJoueurTorpilleur
			// 
			this.lblJoueurTorpilleur.AutoSize = true;
			this.lblJoueurTorpilleur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurTorpilleur.Location = new System.Drawing.Point(9, 638);
			this.lblJoueurTorpilleur.Name = "lblJoueurTorpilleur";
			this.lblJoueurTorpilleur.Size = new System.Drawing.Size(160, 20);
			this.lblJoueurTorpilleur.TabIndex = 5;
			this.lblJoueurTorpilleur.Text = "5. Torpilleur (2 coups)";
			// 
			// lblJoueurSousMarin
			// 
			this.lblJoueurSousMarin.AutoSize = true;
			this.lblJoueurSousMarin.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurSousMarin.Location = new System.Drawing.Point(9, 613);
			this.lblJoueurSousMarin.Name = "lblJoueurSousMarin";
			this.lblJoueurSousMarin.Size = new System.Drawing.Size(177, 20);
			this.lblJoueurSousMarin.TabIndex = 4;
			this.lblJoueurSousMarin.Text = "4. Sous-marin (3 coups)";
			// 
			// lblJoueurCroiseur
			// 
			this.lblJoueurCroiseur.AutoSize = true;
			this.lblJoueurCroiseur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurCroiseur.Location = new System.Drawing.Point(9, 586);
			this.lblJoueurCroiseur.Name = "lblJoueurCroiseur";
			this.lblJoueurCroiseur.Size = new System.Drawing.Size(155, 20);
			this.lblJoueurCroiseur.TabIndex = 3;
			this.lblJoueurCroiseur.Text = "3. Croiseur (3 coups)";
			// 
			// lblJoueurCuirassé
			// 
			this.lblJoueurCuirassé.AutoSize = true;
			this.lblJoueurCuirassé.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurCuirassé.Location = new System.Drawing.Point(9, 561);
			this.lblJoueurCuirassé.Name = "lblJoueurCuirassé";
			this.lblJoueurCuirassé.Size = new System.Drawing.Size(158, 20);
			this.lblJoueurCuirassé.TabIndex = 2;
			this.lblJoueurCuirassé.Text = "2. Cuirassé (4 coups)";
			// 
			// lblJoueurPorteAvions
			// 
			this.lblJoueurPorteAvions.AutoSize = true;
			this.lblJoueurPorteAvions.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblJoueurPorteAvions.Location = new System.Drawing.Point(9, 537);
			this.lblJoueurPorteAvions.Name = "lblJoueurPorteAvions";
			this.lblJoueurPorteAvions.Size = new System.Drawing.Size(184, 20);
			this.lblJoueurPorteAvions.TabIndex = 1;
			this.lblJoueurPorteAvions.Text = "1. Porte-avions (5 coups)";
			// 
			// vaiJoueur
			// 
			this.vaiJoueur.BackColor = System.Drawing.Color.Transparent;
			this.vaiJoueur.BackgroundImage = global::BatailleNavale.Properties.Resources.mer3_475X475;
			this.vaiJoueur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.vaiJoueur.CellAppearance.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiJoueur.CellAppearance.ImageList = this.imgCase;
			this.vaiJoueur.CellAppearance.TextColor = System.Drawing.Color.Black;
			this.vaiJoueur.CellSize = new System.Drawing.Size(40, 40);
			this.vaiJoueur.ColumnCount = 10;
			this.vaiJoueur.ColumnHeader.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiJoueur.ColumnHeader.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.vaiJoueur.ColumnHeader.ForeColor = System.Drawing.Color.Black;
			this.vaiJoueur.ColumnHeader.Style = VisualArrays.enuHeaderBkgStyle.None;
			this.vaiJoueur.ColumnHeader.ValueStyle = VisualArrays.enuDataStyle.IndexBase1;
			this.vaiJoueur.ColumnHeader.Visible = true;
			this.vaiJoueur.GridAppearance.Color = System.Drawing.Color.Black;
			this.vaiJoueur.GridAppearance.Margin = 3;
			this.vaiJoueur.Location = new System.Drawing.Point(9, 30);
			this.vaiJoueur.Name = "vaiJoueur";
			this.vaiJoueur.RowCount = 10;
			this.vaiJoueur.RowHeader.BackgroundColor = System.Drawing.Color.Transparent;
			this.vaiJoueur.RowHeader.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.vaiJoueur.RowHeader.ForeColor = System.Drawing.Color.Black;
			this.vaiJoueur.RowHeader.Style = VisualArrays.enuHeaderBkgStyle.None;
			this.vaiJoueur.RowHeader.ValueStyle = VisualArrays.enuDataStyle.User;
			this.vaiJoueur.RowHeader.Visible = true;
			this.vaiJoueur.Size = new System.Drawing.Size(477, 477);
			this.vaiJoueur.TabIndex = 0;
			this.vaiJoueur.View = VisualArrays.enuIntView.ImageList;
			// 
			// btnDémarrer
			// 
			this.btnDémarrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDémarrer.Location = new System.Drawing.Point(757, 626);
			this.btnDémarrer.Name = "btnDémarrer";
			this.btnDémarrer.Size = new System.Drawing.Size(246, 50);
			this.btnDémarrer.TabIndex = 2;
			this.btnDémarrer.Text = "&Démarrer";
			this.btnDémarrer.UseVisualStyleBackColor = true;
			this.btnDémarrer.Click += new System.EventHandler(this.btnDémarrer_Click);
			// 
			// btnRecommencer
			// 
			this.btnRecommencer.Enabled = false;
			this.btnRecommencer.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnRecommencer.Location = new System.Drawing.Point(508, 626);
			this.btnRecommencer.Name = "btnRecommencer";
			this.btnRecommencer.Size = new System.Drawing.Size(243, 50);
			this.btnRecommencer.TabIndex = 3;
			this.btnRecommencer.Text = "&Recommencer";
			this.btnRecommencer.UseVisualStyleBackColor = true;
			this.btnRecommencer.Click += new System.EventHandler(this.btnRecommencer_Click);
			// 
			// frmBatailleNavale
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ClientSize = new System.Drawing.Size(1012, 681);
			this.Controls.Add(this.btnRecommencer);
			this.Controls.Add(this.btnDémarrer);
			this.Controls.Add(this.grpOrdinateur);
			this.Controls.Add(this.grpJoueur);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmBatailleNavale";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Bataille navale";
			this.Load += new System.EventHandler(this.frmBatailleNavale_Load);
			this.grpOrdinateur.ResumeLayout(false);
			this.grpOrdinateur.PerformLayout();
			this.grpJoueur.ResumeLayout(false);
			this.grpJoueur.PerformLayout();
			this.grpJoueurAttaques.ResumeLayout(false);
			this.grpJoueurAttaques.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpOrdinateur;
        private System.Windows.Forms.GroupBox grpJoueur;
        private VisualArrays.VisualIntArray vaiJoueur;
        private VisualArrays.VisualIntArray vaiOrdinateur;
        private System.Windows.Forms.ImageList imgCase;
        private System.Windows.Forms.CheckBox chkJoueurTorpilleur;
        private System.Windows.Forms.CheckBox chkJoueurSousMarin;
        private System.Windows.Forms.CheckBox chkJoueurCroiseur;
        private System.Windows.Forms.CheckBox chkJoueurCuirassé;
        private System.Windows.Forms.Label lblJoueurVertical;
        private System.Windows.Forms.CheckBox chkJoueurPorteAvions;
        private System.Windows.Forms.Label lblCoord;
        private System.Windows.Forms.TextBox txtJoueurTorpilleur;
        private System.Windows.Forms.TextBox txtJoueurSousMarin;
        private System.Windows.Forms.TextBox txtJoueurCroiseur;
        private System.Windows.Forms.TextBox txtJoueurCuirassé;
        private System.Windows.Forms.TextBox txtJoueurPorteAvions;
        private System.Windows.Forms.Label lblJoueurTorpilleur;
        private System.Windows.Forms.Label lblJoueurSousMarin;
        private System.Windows.Forms.Label lblJoueurCroiseur;
        private System.Windows.Forms.Label lblJoueurCuirassé;
        private System.Windows.Forms.Label lblJoueurPorteAvions;
        private System.Windows.Forms.Button btnDémarrer;
        private System.Windows.Forms.Button btnRecommencer;
        private System.Windows.Forms.Label lblOrdiAttaqueCoordonnée;
        private System.Windows.Forms.Label lblOrdiAttaqueStatut;
        private System.Windows.Forms.Label lblOrdiStatut;
        private System.Windows.Forms.Label lblOrdiAttaqueCoordonnéeStatique;
        private System.Windows.Forms.GroupBox grpJoueurAttaques;
        private System.Windows.Forms.Label lblJoueurAttaqueStatut;
        private System.Windows.Forms.Label lblJoueurAttaqueStatutStatique;
        private System.Windows.Forms.TextBox txtJoueurAttaquesCoord;
        private System.Windows.Forms.Label lblJoueurAttCoordonnée;
        private System.Windows.Forms.RadioButton optOrdiIntelligent;
        private System.Windows.Forms.RadioButton optOrdiHasard;
        private System.Windows.Forms.Label lblOrdiMode;
        private System.Windows.Forms.Label lblOrdiTorpilleur;
        private System.Windows.Forms.Label lblOrdiSousMarin;
        private System.Windows.Forms.Label lblOrdiCroiseur;
        private System.Windows.Forms.Label lblOrdiCuirassé;
        private System.Windows.Forms.Label lblOrdiPorteAvions;

    }
}

