﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisualArrays;

namespace BatailleNavale
{
	/// <summary>
	/// Enum représente ce que joueur doit faire
	/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
	/// Date: 11 Novembre 2014, Jour du souvenir
	/// </summary>
	enum Joueur
	{
		REEL,
		ORDINATEUR
	}

	/// <summary>
	/// Enum représantant les différents bateau
	/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
	/// Date: 11 Novembre 2014, Jour du Souvenir
	/// </summary>
	enum Bateau
	{
		AUCUN = 0,
		PORTEAVION = 1,
		CUIRASSE = 2,
		CROISEUR = 3,
		SOUSMARIN = 4,
		TORPILLEUR = 5
	}

    /// <summary>
    /// Classe servant à la gestion des actions et de la logique du jeu de Bataille Navale
	/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
	/// Date: 11 Novembre 2014, Jour du Souvenir
    /// </summary>
    public partial class frmBatailleNavale : Form
    {
        #region Constantes

		const int LONGUEUR = 10;
		const int HAUTEUR = 10;

		const int ID_VIDE = 0;
		const int ID_ALEAU = 1;
		const int ID_TOUCHE = 2;
		const int ID_TOUCHESANSBATEAU = 3;
		const int ID_BATEAU = 4;
		const int GROSSEUR_ENUM_BATEAU = 6;

		// Accesseur X et Y des tableaux-coord et de la longueur
		const int X = 1;
		const int Y = 0;
		const int LONGUEUR_BATEAU = 2;	// Identificateur vers la longueur du bateaux placer aux coord X et Y

        #endregion

        #region Champs (Variables globales)

        // Déclaration des variables globales (champs).
		bool _bJeuCommencer = false;

        Random _rnd = new Random();

		Bateau[,] _aiBateauxJoueur;
		Bateau[,] _aiBateauxOrdi;

		bool[] _abBateauPlace = new bool[GROSSEUR_ENUM_BATEAU];		// Est-ce que le bateau [INDEX DU BATEAU] a été placé?
		bool[] _abBateauxVertical = new bool[GROSSEUR_ENUM_BATEAU];	// Est-ce que le bateau était vertical loras de son placement?
		bool[] _abBateauxJoueurCouler = new bool[GROSSEUR_ENUM_BATEAU];
		bool[] _abBateauxOrdiCouler = new bool[GROSSEUR_ENUM_BATEAU];

        int[] _aiViesBateauxOrdinateur = new int[GROSSEUR_ENUM_BATEAU];
        int[] _aiViesBateauxJoueur = new int[GROSSEUR_ENUM_BATEAU];

		int[,] _aiCoordBateaux = new int[GROSSEUR_ENUM_BATEAU, 3];	// _aiCoordBateau[ID_BATEAU,X/Y/LONGUEUR] sert lors du placement des bateaux au cas où le joueur veut changé la place d'un bateau déjà placé

        #endregion

		#region Propriétés

		/// <summary>
		/// Propriété retournant true quand tout les bateaux ont ete place
		/// Auteurs: Nicolas Lapointe et Jonathan Gauvin
		/// Date: 17 Novembre 2014
		/// </summary>
		private bool BateauxJoueurPlacés
		{
			get
			{
				bool bRet = true;
				foreach (bool b in _abBateauPlace)
					bRet &= b;

				return bRet;
			}
		}

		/// <summary>
		/// Renvoie true si tous les bateaux du joueur sont coulés
		/// Aut:
		/// Dat:25Nov14
		/// </summary>
		private bool BateauxJoueurCoulés
		{
			get
			{
				bool bRet = true;
				foreach (bool b in _abBateauxJoueurCouler)
					bRet &= b;

				return bRet;
			}
		}

		/// <summary>
		/// Renvoie true si tous les bateaux du joueur sont coulés
		/// Aut:
		/// Dat:25Nov14
		/// </summary>
		private bool BateauxOrdiCoulés
		{
			get
			{
				bool bRet = true;
				foreach (bool b in _abBateauxOrdiCouler)
					bRet &= b;

				return bRet;
			}
		}

		#endregion

		#region Événements spéciaux
		/// <summary>
        /// Auteur: Martin Lalancette
        /// Description: L'ajout de ce code permet d'afficher une lettre dans l'en-tête de ligne.
        /// Date: 2014-10-16
        /// </summary>
        /// <param name="sender">Qui a appelé l'événement</param>
        /// <param name="e">Les paramètres passés lors de l'appel.</param>
        private void frmBatailleNavale_Load(object sender, EventArgs e)
        {
            // Permet de mettre des lettres dans les en-tête de lignes.
            char cCar = 'A';
            for (int iIndex = 0; iIndex < vaiJoueur.RowCount; iIndex++)
            {
                vaiJoueur.RowHeaderArray[iIndex] = cCar.ToString();
                vaiOrdinateur.RowHeaderArray[iIndex] = cCar.ToString();
                cCar++;
            }
        }
        #endregion

        #region Méthodes
        /// <summary>
        /// Auteur: Martin Lalancette + Nicolas Lapointe et Jonathan Gauvin
        /// Description: Initialise les composantes visuelles du formulaire.
        /// Date: 2014-10-16
        /// </summary>
        public frmBatailleNavale()
        {
            InitializeComponent();
			reset(); // Première initialisation
        }

		/// <summary>
		/// Opérations d'initialisation et de remise à zéro
		/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
		/// Date: 14 Novembre 2014
		/// </summary>
		private void reset()
		{
			//Remise à zéro des tableaux et valeurs
			_aiBateauxJoueur = new Bateau[HAUTEUR, LONGUEUR];
			_aiBateauxOrdi = new Bateau[HAUTEUR, LONGUEUR];
			_aiCoordBateaux = new int[GROSSEUR_ENUM_BATEAU, 3];
			_abBateauPlace = new bool[GROSSEUR_ENUM_BATEAU];
			_abBateauxVertical = new bool[GROSSEUR_ENUM_BATEAU];
            _aiViesBateauxOrdinateur = new int[GROSSEUR_ENUM_BATEAU];
            _aiViesBateauxJoueur = new int[GROSSEUR_ENUM_BATEAU];

			for (int i = 0; i < _aiBateauxJoueur.GetLength(0); i++)
				for (int j = 0; j < _aiBateauxJoueur.GetLength(1); j++)
				{
					_aiBateauxJoueur[i, j] = Bateau.AUCUN;
					_aiBateauxOrdi[i, j] = Bateau.AUCUN; // Les deux tableau ont les mêmes dimensions
				}
			
			for(int i = 0; i < vaiJoueur.RowCount; i++)
				for(int j = 0; j < vaiJoueur.ColumnCount; j++)
				{
					vaiJoueur[i, j] = ID_VIDE;
					vaiOrdinateur[i, j] = ID_VIDE;
				}
			for (int i = 0; i < _aiCoordBateaux.GetLength(0); i++)
				for (int j = 0; j < _aiCoordBateaux.GetLength(1); j++)	// [i,X] = -1; [i,Y] = -1; [i,LONGUEUR_BATEAU] = -1
					_aiCoordBateaux[i, j] = 0;

			for (int i = 0; i < _abBateauPlace.Length; i++)
				_abBateauPlace[i] = false;

			_abBateauPlace[(int)Bateau.AUCUN] = true; // Doit etre tout le temps a vrai pour déterminer si tous les autres bateaux on été plaçés


			for (int i = 0; i < _abBateauxVertical.Length; i++)
				_abBateauxVertical[i] = false;

			for (int i = 0; i < _abBateauxJoueurCouler.Length; i++)
				_abBateauxJoueurCouler[i] = false;

			for (int i = 0; i < _abBateauxOrdiCouler.Length; i++)
				_abBateauxOrdiCouler[i] = false;

            _aiViesBateauxOrdinateur[(int)Bateau.AUCUN] = -1;
            _aiViesBateauxOrdinateur[(int)Bateau.PORTEAVION] = 5;
            _aiViesBateauxOrdinateur[(int)Bateau.CUIRASSE] = 4;
            _aiViesBateauxOrdinateur[(int)Bateau.CROISEUR] = 3;
            _aiViesBateauxOrdinateur[(int)Bateau.SOUSMARIN] = 3;
            _aiViesBateauxOrdinateur[(int)Bateau.TORPILLEUR] = 2;

            _aiViesBateauxJoueur[(int)Bateau.AUCUN] = -1;
            _aiViesBateauxJoueur[(int)Bateau.PORTEAVION] = 5;
            _aiViesBateauxJoueur[(int)Bateau.CUIRASSE] = 4;
            _aiViesBateauxJoueur[(int)Bateau.CROISEUR] = 3;
            _aiViesBateauxJoueur[(int)Bateau.SOUSMARIN] = 3;
            _aiViesBateauxJoueur[(int)Bateau.TORPILLEUR] = 2;
            
			_bJeuCommencer = false;
		}

		/// <summary>
		/// Place le bateau demandé avec les bonnes conditions
		/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
		/// Date: 14 Novembre 2014
		/// </summary>
		/// <param name="joueur">Le joueur propriétaire du bateau</param>
		/// <param name="iID">Le bateau à placer</param>
		/// <param name="iX">Coordonné X</param>
		/// <param name="iY">Coordonné Y</param>
		/// <param name="bVertical">Est-ce Vertical?</param>
		/// <returns>true si le bateau as été placé (il n'y avait pas de bateau dans le chemin )</returns>
		private bool PlacerBateau(Joueur joueur, Bateau iID, int iX, int iY, bool bVertical = false)
		{
			// Déclaration des variables
			int iLongueur = 0;
			
			// Dans quelle direction?
			switch (iID)
			{
				case Bateau.PORTEAVION:
					iLongueur = 5;
					break;
				case Bateau.CUIRASSE:
					iLongueur = 4;
					break;
				case Bateau.CROISEUR:
				case Bateau.SOUSMARIN:
					iLongueur = 3;
					break;
				case Bateau.TORPILLEUR:
					iLongueur = 2;
					break;
				default:
					iLongueur = 0;
					break;
			}

			switch(joueur)
			{
				case Joueur.REEL:
					if (bVertical)
					{
						// Vérifications: est-ce que le bateau est plaçable?
						// Est-ce qu'il dépasse les bords?
						if (iY + iLongueur - 1 >= _aiBateauxJoueur.GetLength(0))
							return false;
						// Est-ce qu'il en croisent un autre
						for (int i = iY; i < iY + iLongueur; i++)
							if (_aiBateauxJoueur[i, iX] != Bateau.AUCUN)
								return false;

						// Placement vertical
						for (int i = iY; i < iY + iLongueur; i++)
						{
							_aiBateauxJoueur[i, iX] = iID;
							vaiJoueur[i, iX] = ID_BATEAU;
						}
					}
					else
					{
						// Vérifications: est-ce que le bateau est plaçable?
						// Est-ce qu'il dépasse les bords?
						if (iX + iLongueur - 1 >= _aiBateauxJoueur.GetLength(1))
							return false;

						// Est-ce qu'il croisent un autre
						for (int i = iX; i < iX + iLongueur; i++)
							if (_aiBateauxJoueur[iY, i] != Bateau.AUCUN)
								return false;

						// Placement horizontal
						for (int i = iX; i < iX + iLongueur; i++)
						{
							_aiBateauxJoueur[iY, i] = iID;
							vaiJoueur[iY, i] = ID_BATEAU;
						}
					}

					// Le bateaux du joueur a été placé, on enregistre ses coordonnées et s'il est vertical ou non
					_aiCoordBateaux[(int)iID, X] = iX;
					_aiCoordBateaux[(int)iID, Y] = iY;
					_aiCoordBateaux[(int)iID, LONGUEUR_BATEAU] = iLongueur;
					_abBateauxVertical[(int)iID] = bVertical;
					_abBateauPlace[(int)iID] = true;
					break;
				case Joueur.ORDINATEUR:
					if (bVertical)
					{
						// Vérifications: est-ce que le bateau est plaçable?
						// Est-ce qu'il dépasse les bords^
						if (iY + iLongueur - 1 >= HAUTEUR)
							return false;

						// Est-ce qu'il croisent un autre
						for (int i = iY; i < iY + iLongueur; i++)
							if (_aiBateauxOrdi[i, iX] != Bateau.AUCUN)
								return false;

						for (int i = iY; i < iY + iLongueur; i++)
							_aiBateauxOrdi[i, iX] = iID;
					}
					else
					{
						// Vérifications: est-ce que le bateau est plaçable?
						// Est-ce qu'il dépasse les bords?
						if (iX + iLongueur - 1 >= _aiBateauxOrdi.GetLength(1))
							return false;

						// Est-ce qu'il croisent un autre
						for (int i = iX; i < iX + iLongueur; i++)
							if (_aiBateauxOrdi[iY, i] != Bateau.AUCUN)
								return false;

						for (int i = iX; i < iX + iLongueur; i++)
							_aiBateauxOrdi[iY, i] = iID;
					}
					break;
			}
			return true;
		}

		/// <summary>
		/// Fonctions s'occupant de placer les bateaux de l'ordinateurs
		/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
		/// Date: 15 Novembre 2014
		/// </summary>
		private void PlacerBateauxOrdi()
		{
			bool bEstPlaceable = false;

			for (int iIdBateau = 1; iIdBateau <= 5; iIdBateau++)
				while(!bEstPlaceable) // Tant que le bateau n'est pas placer/plaçable
				{
					int iX = _rnd.Next(0, LONGUEUR);
					int iY = _rnd.Next(0, HAUTEUR);
					bool bVertical = Convert.ToBoolean(_rnd.Next(0, 2)); //true si 1 false si 0

					bEstPlaceable = PlacerBateau(Joueur.ORDINATEUR, (Bateau)iIdBateau, iX, iY, bVertical);
				}
		}
		
		/// <summary>
		/// Determine si la coordonnee est valide (Format: X00; exemple: A01, E10, C05)
		/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
		/// Date: 15 Novembre 2014
		/// </summary>
		/// <param name="sCoord">La coord version texte a analyser</param>
		/// <returns>False= non Valide; True: Valide</returns>
		private bool CoordValide(string sCoord)
		{
			// La longueur de la coord est pas bonne
			if (sCoord.Length != 3)
				return false;
			// La coord n'est pas au bon format
			switch (sCoord.ToUpper()[0])
			{
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
					if (!char.IsNumber(sCoord[2]))
						return false;
					if (sCoord[1] == '1' && sCoord[2] != '0')
						return false;
					if (sCoord[1] != '1' && sCoord[1] != '0')
						return false;
					break;
				default:
					return false;
			}
			return true;
		}

		/// <summary>
		/// Renvoie la version x et y de la coord donnée en paramètre
		/// Auteurs: Nicolas Lapointe et Jonathan Gauvin
		/// Date: 17 Novembre 2014
		/// </summary>
		/// <param name="sCoord">La coordonnée à convertir</param>
		/// <returns>La coord sous forme de vecteur de int (x, y)</returns>
		private int[] ConvertirCoord(string sCoord)
		{
			// La coordonnée est invalide par défaut
			int[] aiCoordConverti = new int[3] { -1, -1, -1 };

			if (!CoordValide(sCoord))
				return new int[3] { -1, -1, -1 };

			// La coord est valide
			// Conversion y
			switch(sCoord[0])
			{
				case 'A':
					aiCoordConverti[0] = 0;
					break;
				case 'B':
					aiCoordConverti[0] = 1;
					break;
				case 'C':
					aiCoordConverti[0] = 2;
					break;
				case 'D':
					aiCoordConverti[0] = 3;
					break;
				case 'E':
					aiCoordConverti[0] = 4;
					break;
				case 'F':
					aiCoordConverti[0] = 5;
					break;
				case 'G':
					aiCoordConverti[0] = 6;
					break;
				case 'H':
					aiCoordConverti[0] = 7;
					break;
				case 'I':
					aiCoordConverti[0] = 8;
					break;
				case 'J':
					aiCoordConverti[0] = 9;
					break;
				default:
					return new int[3] { -1, -1, -1 };
			}

			// conversion x
			switch (sCoord[2])
			{
				case '1':
					aiCoordConverti[1] = 0;
					break;
				case '2':
					aiCoordConverti[1] = 1;
					break;
				case '3':
					aiCoordConverti[1] = 2;
					break;
				case '4':
					aiCoordConverti[1] = 3;
					break;
				case '5':
					aiCoordConverti[1] = 4;
					break;
				case '6':
					aiCoordConverti[1] = 5;
					break;
				case '7':
					aiCoordConverti[1] = 6;
					break;
				case '8':
					aiCoordConverti[1] = 7;
					break;
				case '9':
					aiCoordConverti[1] = 8;
					break;
				case '0':
					aiCoordConverti[1] = 9;
					break;
				default:
					return new int[3] { -1, -1, -1 };
			}
			// la coord est définie, la partie LONGUEUR_BATEAU peut rester a -1, on l'utilise que plus tard
			return aiCoordConverti;
		}

		/// <summary>
		/// Renvoie la version string de la coord x;y donné en paramètre
		/// AUT:
		/// DAT: 25N14
		/// </summary>
		/// <param name="sCoord"></param>
		/// <returns></returns>
		private string ConvertirCoord(int[] sCoord)
		{
			string sRet = "";
			switch(sCoord[0])
			{
				case 0:
					sRet += 'A';
					break;
				case 1:
					sRet += 'B';
					break;
				case 2:
					sRet += 'C';
					break;
				case 3:
					sRet += 'D';
					break;
				case 4:
					sRet += 'E';
					break;
				case 5:
					sRet += 'F';
					break;
				case 6:
					sRet += 'G';
					break;
				case 7:
					sRet += 'H';
					break;
				case 8:
					sRet += 'I';
					break;
				case 9:
					sRet += 'J';
					break;
				default:
					sRet += 'Z';
					break;
			}

			switch (sCoord[2])
			{
				case 0:
					sRet += "01";
					break;
				case 1:
					sRet += "02";
					break;
				case 2:
					sRet += "03";
					break;
				case 3:
					sRet += "04";
					break;
				case 4:
					sRet += "05";
					break;
				case 5:
					sRet += "06";
					break;
				case 6:
					sRet += "07";
					break;
				case 7:
					sRet += "08";
					break;
				case 8:
					sRet += "09";
					break;
				case 9:
					sRet += "10";
					break;
				default:
					sRet += "99";
					break;
			}

			return sRet;
		}

		/// <summary>
		/// Efface ce bateau déjà placé
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="byId">Le Bateau à effacer</param>
		private void EffacerBateauJoueur(Bateau byId)
		{
			if (_abBateauxVertical[(int)byId])
			{
				// Placement vertical
				int iX = _aiCoordBateaux[(int)byId, X];
				int iY = _aiCoordBateaux[(int)byId, Y];

				for (int i = iY; i < iY + _aiCoordBateaux[(int)byId, LONGUEUR_BATEAU]; i++)
				{
					_aiBateauxJoueur[i, iX] = Bateau.AUCUN;
					vaiJoueur[i, iX] = ID_VIDE;
				}
			}
			else
			{
				// Placement vvertical
				int iX = _aiCoordBateaux[(int)byId, X];
				int iY = _aiCoordBateaux[(int)byId, Y];

				for (int i = iX; i < iX + _aiCoordBateaux[(int)byId, LONGUEUR_BATEAU]; i++)
				{
					_aiBateauxJoueur[iY, i] = Bateau.AUCUN;
					vaiJoueur[iY, i] = ID_VIDE;
				}
			}
		}

		// Retourne true si tous les bateau du joueur ont été placés
        #endregion

        #region Événements
        /// <summary>
        /// L'utilisateur a cliqué sur le bouton démarrer
		/// Auteur(s): Nicolas Lapointe et Jonathan Gauvin
		/// Date: 14 Novembre 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDémarrer_Click(object sender, EventArgs e)
        {
			//Init
			string sMsg = "Le jeu à maintenant commencé!\nVous avez décidé d'affronter ";
			// Si une des coordonnés n'est pas valide on affiche un message avertissant le joueur et on sort de la fonction (on peut pas continuer)
			if (!BateauxJoueurPlacés)
			{
				MessageBox.Show("Les bateaux n'ont pas tous été placés\nVeuillez placer vos bateaux a des coordonnées valides, svp.", "ERREUR", MessageBoxButtons.OK, MessageBoxIcon.Error);
				return;
			}

			// On peut continuer!, placement de bateau ordi et début du jeu!
			PlacerBateauxOrdi();

			_bJeuCommencer = true;
			btnRecommencer.Enabled = true;

			btnDémarrer.Enabled = optOrdiHasard.Enabled = optOrdiIntelligent.Enabled = false;


			if (optOrdiHasard.Checked)
				sMsg += "le hasard, Bon succès!";
			else
				sMsg += "Bob, l'Intelligence Artificielle super-intelligente, bonne chance! Vous en aurez grand besoin!";

			MessageBox.Show(sMsg, "C'est parti!");
        }

        /// <summary>
        /// Placer le porte-avions la ou c'est demandé si la coord est valide
		/// Auteur(s) : Nicolas Lapointe
		/// Date: 23 Novembre 2014
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPorteAvions_TextChanged(object sender, EventArgs e)
        {
			// Le jeu n'est pas encore commencé, on peut donc placé un bateau
			if (!_bJeuCommencer)
			{

				int[] aiCoordValide = ConvertirCoord(txtJoueurPorteAvions.Text);

				if (aiCoordValide[0] == -1)
					return;

				if (_abBateauPlace[(int)Bateau.PORTEAVION])	// Ce bateau a déjà été placé avant, on l'enleve
				{
					EffacerBateauJoueur(Bateau.PORTEAVION);
				}

				// le bateau ne peut être placé
				if (!PlacerBateau(Joueur.REEL, Bateau.PORTEAVION, aiCoordValide[X], aiCoordValide[Y], chkJoueurPorteAvions.Checked))
				{
					if (_abBateauPlace[(int)Bateau.PORTEAVION]) // Si le bateau avait deja été placé avant on le replace
					{
						PlacerBateau(Joueur.REEL, Bateau.PORTEAVION, _aiCoordBateaux[(int)Bateau.PORTEAVION, X], _aiCoordBateaux[(int)Bateau.PORTEAVION, Y], _abBateauxVertical[(int)Bateau.PORTEAVION]);
						txtJoueurPorteAvions.Text = "";
					}
					MessageBox.Show("Le Porte-avion ne peut être placé à l'endroit demandé:\n il croise un autre bateau ou dépasse la grille", "ERREUR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
        }

		/// <summary>
		/// Placer le Cuirassé la ou c'est demandé si la coord est valide
		/// Auteur(s) : Nicolas Lapointe
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void txtCuirassé_TextChanged(object sender, EventArgs e)
        {
			// Le jeu n'est pas encore commencé, on peut donc placé un bateau
			if (!_bJeuCommencer)
			{

				int[] aiCoordValide = ConvertirCoord(txtJoueurCuirassé.Text);

				if (aiCoordValide[0] == -1)
					return;

				if (_abBateauPlace[(int)Bateau.CUIRASSE])	// Ce bateau a déjà été placé avant, on l'enleve
				{
					EffacerBateauJoueur(Bateau.CUIRASSE);
				}

				// le bateau ne peut être placé
				if (!PlacerBateau(Joueur.REEL, Bateau.CUIRASSE, aiCoordValide[X], aiCoordValide[Y], chkJoueurCuirassé.Checked))
				{
					if (_abBateauPlace[(int)Bateau.CUIRASSE]) // Si le bateau avait deja été placé avant on le replace
					{
						PlacerBateau(Joueur.REEL, Bateau.CUIRASSE, _aiCoordBateaux[(int)Bateau.CUIRASSE, X], _aiCoordBateaux[(int)Bateau.CUIRASSE, Y], _abBateauxVertical[(int)Bateau.CUIRASSE]);
						txtJoueurPorteAvions.Text = "";
					}
					MessageBox.Show("Le Cuirassé ne peut être placé à l'endroit demandé:\n il croise un autre bateau ou dépasse la grille", "ERREUR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
        }

		/// <summary>
		/// Placer le Croiseur la ou c'est demandé si la coord est valide
		/// Auteur(s) : Nicolas Lapointe
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void txtCroiseur_TextChanged(object sender, EventArgs e)
        {
			// Le jeu n'est pas encore commencé, on peut donc placé un bateau
			if (!_bJeuCommencer)
			{

				int[] aiCoordValide = ConvertirCoord(txtJoueurCroiseur.Text);

				if (aiCoordValide[0] == -1)
					return;

				if (_abBateauPlace[(int)Bateau.CROISEUR])	// Ce bateau a déjà été placé avant, on l'enleve
				{
					EffacerBateauJoueur(Bateau.CROISEUR);
				}

				// le bateau ne peut être placé
				if (!PlacerBateau(Joueur.REEL, Bateau.CROISEUR, aiCoordValide[X], aiCoordValide[Y], chkJoueurCroiseur.Checked))
				{
					if (_abBateauPlace[(int)Bateau.CROISEUR]) // Si le bateau avait deja été placé avant on le replace
					{
						PlacerBateau(Joueur.REEL, Bateau.CROISEUR, _aiCoordBateaux[(int)Bateau.CROISEUR, X], _aiCoordBateaux[(int)Bateau.CROISEUR, Y], _abBateauxVertical[(int)Bateau.CROISEUR]);
						txtJoueurPorteAvions.Text = "";
					}
					MessageBox.Show("Le Croiseur ne peut être placé à l'endroit demandé:\n il croise un autre bateau ou dépasse la grille", "ERREUR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
        }

		/// <summary>
		/// Placer le Sous-Marin la ou c'est demandé si la coord est valide
		/// Auteur(s) : Nicolas Lapointe
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void txtSousMarin_TextChanged(object sender, EventArgs e)
        {
			// Le jeu n'est pas encore commencé, on peut donc placé un bateau
			if (!_bJeuCommencer)
			{

				int[] aiCoordValide = ConvertirCoord(txtJoueurSousMarin.Text);

				if (aiCoordValide[0] == -1)
					return;

				if (_abBateauPlace[(int)Bateau.SOUSMARIN])	// Ce bateau a déjà été placé avant, on l'enleve
				{
					EffacerBateauJoueur(Bateau.SOUSMARIN);
				}

				// le bateau ne peut être placé
				if (!PlacerBateau(Joueur.REEL, Bateau.SOUSMARIN, aiCoordValide[X], aiCoordValide[Y], chkJoueurSousMarin.Checked))
				{
					if (_abBateauPlace[(int)Bateau.SOUSMARIN]) // Si le bateau avait deja été placé avant on le replace
					{
						PlacerBateau(Joueur.REEL, Bateau.SOUSMARIN, _aiCoordBateaux[(int)Bateau.SOUSMARIN, X], _aiCoordBateaux[(int)Bateau.SOUSMARIN, Y], _abBateauxVertical[(int)Bateau.SOUSMARIN]);
						txtJoueurPorteAvions.Text = "";
					}
					MessageBox.Show("Le Sous-Marin ne peut être placé à l'endroit demandé:\n il croise un autre bateau ou dépasse la grille", "ERREUR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
        }

		/// <summary>
		/// Placer le Torpilleur la ou c'est demandé si la coord est valide
		/// Auteur(s) : Nicolas Lapointe
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void txtTorpilleur_TextChanged(object sender, EventArgs e)
        {
			// Le jeu n'est pas encore commencé, on peut donc placé un bateau
			if (!_bJeuCommencer)
			{

				int[] aiCoordValide = ConvertirCoord(txtJoueurTorpilleur.Text);

				if (aiCoordValide[0] == -1)
					return;

				if (_abBateauPlace[(int)Bateau.TORPILLEUR])	// Ce bateau a déjà été placé avant, on l'enleve
				{
					EffacerBateauJoueur(Bateau.TORPILLEUR);
				}

				// le bateau ne peut être placé
				if (!PlacerBateau(Joueur.REEL, Bateau.TORPILLEUR, aiCoordValide[X], aiCoordValide[Y], chkJoueurTorpilleur.Checked))
				{
					if (_abBateauPlace[(int)Bateau.TORPILLEUR]) // Si le bateau avait deja été placé avant on le replace
					{
						PlacerBateau(Joueur.REEL, Bateau.TORPILLEUR, _aiCoordBateaux[(int)Bateau.TORPILLEUR, X], _aiCoordBateaux[(int)Bateau.TORPILLEUR, Y], _abBateauxVertical[(int)Bateau.TORPILLEUR]);
						txtJoueurPorteAvions.Text = "";
					}
					MessageBox.Show("Le Torpilleur ne peut être placé à l'endroit demandé:\n il croise un autre bateau ou dépasse la grille", "ERREUR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
					return;
				}
			}
        }

        /// <summary>
        /// Le checkbox du Porte-avion à changé d'état
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkPorteAvions_CheckedChanged(object sender, EventArgs e)
        {
			txtPorteAvions_TextChanged(sender, e);
        }

		/// <summary>
		/// Le checkbox du Cuirassé à changé d'état
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void chkCuirassé_CheckedChanged(object sender, EventArgs e)
        {
			txtCuirassé_TextChanged(sender, e);
        }

		/// <summary>
		/// Le checkbox du Croiseur à changé d'état
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void chkCroiseur_CheckedChanged(object sender, EventArgs e)
        {
			txtCroiseur_TextChanged(sender, e);
        }

		/// <summary>
		/// Le checkbox du Sous-Marin à changé d'état
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void chkSousMarin_CheckedChanged(object sender, EventArgs e)
        {
			txtSousMarin_TextChanged(sender, e);
        }

		/// <summary>
		/// Le checkbox du Torpilleur à changé d'état
		/// Auteur(s) : Nicolas Lapointe et Jonathan Gauvin
		/// Date: 23 Novembre 2014
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void chkTorpilleur_CheckedChanged(object sender, EventArgs e)
        {
			txtTorpilleur_TextChanged(sender, e);
        }

        // Ajouter les commentaires (Auteur, Description, Date)
        private void btnRecommencer_Click(object sender, EventArgs e)
        {
			if (MessageBox.Show("VOulez-vous vraiment recommencer?", "Êtes-vous sûr ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
			{
				reset();
				txtJoueurPorteAvions.Text = txtJoueurCuirassé.Text = txtJoueurCroiseur.Text = txtJoueurSousMarin.Text = txtJoueurTorpilleur.Text = "";
				btnRecommencer.Enabled = false;
				btnDémarrer.Enabled = optOrdiHasard.Enabled = optOrdiIntelligent.Enabled = true;
			}

        }

        /// <summary>
        /// Les attaques
		/// AUT:
		/// DAT:25N14
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void vaiOrdinateur_CellMouseClick(object sender, CellMouseEventArgs e)
        {
            // Pour obtenir les coordonnées de la grille, utilisez les propriétés e.Row (Y) et e.Column (X).
			// Déclaration/init variables
			int iy = e.Row;
			int ix = e.Column;
			bool bAttaqueReussi = false;	// Pour l'attaque ordi


			// Rien faire: le jeu n'est pas commencé!
			if (!_bJeuCommencer)
				return;
            //Attaque de l'ordinateur
            //Variables

            while (!bAttaqueReussi)
            {
                iy = _rnd.Next(0, 10);
                ix = _rnd.Next(0, 10);

                if (vaiJoueur[iy,ix] == ID_VIDE || vaiJoueur[iy,ix] == ID_BATEAU)
                {
					_aiViesBateauxJoueur[(int)_aiBateauxJoueur[iy, ix]]--;
					if(_aiViesBateauxJoueur[(int)_aiBateauxJoueur[iy, ix]] < 0)
					{
						lblOrdiAttaqueStatut.Text = "À l'eau!";
						lblOrdiAttaqueStatut.BackColor = Color.Green;
						vaiJoueur[iy, ix] = ID_ALEAU;
					}
					else if (_aiViesBateauxJoueur[(int)_aiBateauxJoueur[iy, ix]] == 0)
                    {
                        lblOrdiAttaqueStatut.Text = "Coulé!";
                        lblOrdiAttaqueStatut.BackColor = Color.Red;
						vaiJoueur[iy, ix] = ID_TOUCHE;
						_abBateauxJoueurCouler[(int)_aiBateauxJoueur[iy, ix]] = true;
                    }
                    else
                    {
                        lblOrdiAttaqueStatut.Text = "Touché!";
						lblOrdiAttaqueStatut.BackColor = Color.OrangeRed;
						vaiJoueur[iy, ix] = ID_TOUCHE;
                    }
                    //_aiBateauxOrdi[iHorizontal, iVertical] = void;
					bAttaqueReussi = true;
                }
            }
        }
        #endregion
    }
}